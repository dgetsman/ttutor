/*
 * by: Damon Getsman
 * started on: 26apr20 22:21
 * finished:
 *
 * Doing some work to test out a bit of work that I'm doing on revamping the
 * previous ncurses routines that I'd written with the typing tutor project in
 * mind, just to feel around C/C++ again more since it's been so long.
 */

#include <ncurses.h>

#include "dncurses-routines.h"

/*const int MaxCols = (int)getmaxx();
const int MaxLines = (int)getmaxy();
const bool ColorCapable = (bool)has_colors();*/

//just going with a major rewrite of what wasn't working in dncurses-routines,
//so instead of trying work with the code above I'm reworking what's here, as
//well

int main(void) {
  WINDOW *priscr = initscr();	//initialize curses library
  keypad(priscr, TRUE);	//not sure what this does with keyboard mapping :P
  (void) nonl();
  (void) noecho();
  (void) halfdelay(25);	//wait 2.5sec before carrying on w/out input to
			//getch()

  
  if (!has_colors()) {
    #ifdef DEBUGGING
	mvaddstr(12, 15, "No color capability detected!");
	mvaddstr(13, 20, "Hit a key, por favor . . .");
    #endif
  } else {
    #ifdef DEBUGGING
	//working with the capabilities that we normally like here
	mvaddstr(12, 15, "Detected full color capability.");
    #endif
  }

  refresh();
  (void) getch();

  //clear();
  //refresh();
  exit_ncurses(priscr);

  return(0);
}

