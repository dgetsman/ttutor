/*
 * by: Damon Getsman
 * started on: 19mar14 6:08am
 * finished:
 * A few ncurses routines that I've found to be useful for more than one chunk
 * o' code that I'm working wif
 */

#include <curses.h>
#include <string.h>
#include <unistd.h>

/*
 * defined constants
 */

#define MAX_LEN         78

/*
 * useful definitions/flags
 */

#define DEBUGGING
#define VERBOSE         1

/* structures & enumerations */
enum colorpairs_bb {
        RB, GB, YB, BB, CB, MB, WB
} PaintColor = BB;

typedef enum { FULL, HALF } pauseType;

/*
 * function definitions
 */
void exit_ncurses(WINDOW *priscr) {
  //just shut everything down nicely so that we don't get stuck having to
  //manually call 'reset' when we're back at the shell pointer
  clear();
  refresh();

  nl();
  echo();

  //endwin(priscr);
  endwin();	//so how do we do this for any particular window?
		//(should that ever turn out to be necessary)
}



bool init_ncurses(/*WINDOW *priscr*/) {
  /*
   * curses setup for initialization utilizing a screen that will have a half-
   * second pause when displaying any error message regarding inability to
   * init curses mode/capability when started up
   */

  /*priscr = */ initscr();   /* init curses library */
  //keypad(priscr, TRUE); /* keyboard mapping */
  keypad(stdscr, TRUE);	/* changed this when reworking init_ncurses to work
			   without the window being passed, though this will
			   come with more issues in the future */
  (void) nonl();        /* no NL->CR/NL on output */
  (void) noecho();      /* do not echo input */
  (void) halfdelay(25); /* wait 2.5 sec before carrying on w/out input to 
			   getch() */

  /* ouahful variable */
  bool guhupdown;

  #ifdef DEBUGGING
        mvaddstr(0, 0, "In init_ncurses");
  #endif

  guhupdown = has_colors();
  if (guhupdown) {
        /* init color subsystem */
        start_color();

        init_pair(0, COLOR_RED, COLOR_BLACK);
        init_pair(1, COLOR_GREEN, COLOR_BLACK);
        init_pair(2, COLOR_YELLOW, COLOR_BLACK);
        init_pair(3, COLOR_BLUE, COLOR_BLACK);
        init_pair(4, COLOR_CYAN, COLOR_BLACK);
        init_pair(5, COLOR_MAGENTA, COLOR_BLACK);
        init_pair(6, COLOR_WHITE, COLOR_BLACK);

        return true;
  } else {
        mvaddstr(12, 15, "No color capability detected!");
        mvaddstr(13, 20, "Hit a key, por favor . . .");

        refresh();
        (void) getch();

        clear();
        refresh();

	return false;
  }
}

